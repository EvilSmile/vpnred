VPN RED README
====================================

Openvpn is configured so that every client gets mapped to a public ip address.
There is a pool of available public ip addresses (defined by puppet variable
$public_network). When openvpn runs out of available public ip addresses, then
additionall vpn connections get set up with NAT routing, so they can surf the
internet but they will not be able to accept incoming connections. If
$public_network is undefined, then all routes will be NAT.

The magic for this mapping happens in "vpn-route". This script uses
transactional locks on the file that stores the mapping of public ips to vpn
ips, so there should not be any race conditions. You can inspect the current ip
allocation table with this command:

/etc/openvpn/show-pool

This mapping table has no real consequence: it is simply used for record keeping
so that we know when there are available public ips. The real mapping happens in
iptables. If the pool file is missing (/var/run/vpnred-public-ip-pool) then
a new one will get created and seeded with whatever real data is in the
iptables. 

DEBUGGING
-------------------------------

/etc/init.d/openvpn stop
puppetd --disable
openvpn --verb 5 --config vpnred-service.conf

OPENVPN CONFIGURATION COMMENTS
-------------------------------

dev tun
  easier than tap

keepalive 20 120
  keeps the connection from dying due to inactivity.

topology subnet
  allocates only a single ip per client, instead of 4.

script-security 2
  allows client-connect scripts

learn-address /etc/openvpn/vpn-route
  where the networking is setup and taken down for a client connection

push "redirect-gateway def1"
  makes the client send all their traffic to the server.

push "dhcp-option DNS 198.252.153.28"
  tells the client to use our dns server. 
  note: if using MASQUERADE, push the vpn ip for dns. If using SNAT, push the
  public ip.

client-cert-not-required
  don't use client certs. the clients are not 'trusted'

plugin /usr/lib/openvpn/openvpn-auth-pam.so openvpn
  use pam for password authentication, service named 'openvpn' (ie whatever is
  in /etc/pam.d/openvpn)

username-as-common-name
  not sure of the benefit. we don't have a common-name (because there is no
  client cert) so this seemed like a good idea.

duplicate-cn
  allows more than one simultaneous connection per login/pass.

client-config-dir /etc/openvpn/clients
  allows us to have per-user configuration options.
  joe_hill's options are in /etc/openvpn/clients/joe_hill

persist-*
  without persist-x configuration options, when openvpn hits the inactivity
  timeout it will reset x. this is particular bad in the case of persist-tun:
  without it, a timeout will run the disconnect script which will kill the
  network interface.

ISSUES
---------------------

TOPOLOGY

The default topology is 'net30'. This allocates a /30 network for each
connecting client (4 ip addresses). The reason for this is compatibility with
windows clients. 

We don't like this, because it makes it harder to map vpn IP to public IP.
Certainly, it can still be done, but it is much more complicated.

There are two options instead of net30:

(1) use p2p topology: this is incompatible with many windows clients.
(2) use subnet topology: this is incompatible with any client below openvpn 2.1

Both these options allocate just a single ip for each vpn client, making it easy
to map vpn ip to public ip.

For now, we have chosen 'subnet', since most clients now support openvpn 2.1. 

TODO / PROBLEMS
-----------------------

DISABLE PUBLIC IP

Currently, there is no way to disable the public ip feature on a per client
basis. It would be nice if we could allow some clients to disable this and share
an IP.

IP BLOCKS

It would be easy to add multiple discontinuous public ip blocks, but currently
only one per server is supported.

TCP

Now that we have a locking ip pool, we can run multiple openvpn servers, one
bound to tcp and one to udp.

