#!/usr/bin/env ruby

interface = 'eth0'

# ['PREROUTING', 'POSTROUTING'].each do |chain|
#    `/sbin/iptables -t nat --list-rules #{chain} | grep #{vpn_ip}`.split("\n").each do |rule|
#      `/sbin/iptables -t nat #{rule.sub('-A', '-D')}`
#     end
#  end

`ip addr ls dev #{interface} | grep secondary | cut -d' ' -f 6`.split("\n").each do |ip|
  `ip addr del #{ip.strip} dev #{interface}`
end

