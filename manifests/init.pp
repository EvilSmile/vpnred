class vpnred {

  package { [openvpn, ruby, sipcalc]:
    ensure => installed;
  }

  ##
  ## OPENVPN DAEMON
  ##

  file {
    '/var/log/openvpn':
      ensure => directory,
      mode   => '0700',
      owner  => root,
      group  => root;

    '/etc/openvpn/clients':
      source  => 'puppet:///modules/vpnred/openvpn/clients',
      recurse => true,
      mode    => '0755',
      owner   => root,
      group   => root;

    '/etc/openvpn/vpnred-service.conf':
      source => 'puppet:///modules/vpnred/openvpn/vpnred-service.conf',
      mode   => '0644',
      owner  => root,
      group  => root;

    '/etc/openvpn/vpnred-service-tcp.conf':
      source => 'puppet:///modules/vpnred/openvpn/vpnred-service-tcp.conf',
      mode   => '0644',
      owner  => root,
      group  => root;

    '/etc/openvpn/network.cnf':
      content => template('vpnred/network.cnf'),
      mode    => '0644',
      owner   => root,
      group   => root;

    # must be executable
    '/etc/openvpn/vpn-route':
      source => 'puppet:///modules/vpnred/openvpn/vpn-route',
      mode   => '0755',
      owner  => root,
      group  => root;

    # must be executable
    '/etc/openvpn/server-up.sh':
      content => template('vpnred/server-up.sh'),
      mode    => '0755',
      owner   => root,
      group   => root;

    # must be executable
    '/etc/openvpn/server-down.sh':
      content => template('vpnred/server-down.sh'),
      mode    => '0755',
      owner   => root,
      group   => root;

    '/etc/openvpn/show-pool':
      source => 'puppet:///modules/vpnred/openvpn/show-pool',
      mode   => '0755',
      owner  => root,
      group  => root;

    '/etc/openvpn/show-iptables':
      source => 'puppet:///modules/vpnred/openvpn/show-iptables',
      mode   => '0755',
      owner  => root,
      group  => root;

    '/usr/local/share/munin-plugins/vpn_users':
      source => 'puppet:///modules/vpnred/munin/vpn_users',
      mode   => '0755',
      owner  => root,
      group  => root;

    '/etc/openvpn/restartvpn':
      source => 'puppet:///modules/vpnred/restartvpn',
      mode   => '0755',
      owner  => root,
      group  => root;

    '/etc/cron.d/restartvpn':
      content => '0 15 * * 1,3,6 root /etc/openvpn/restartvpn > /dev/null',
      mode    => '0644',
      owner   => root,
      group   => root;
  }

  sysctl::config {
    'net.core.default_qdisc':
      value   => 'fq_codel',
      comment => 'use modern queuing for better latency';
  }

  munin::plugin { 'vpn_users':
    ensure         => 'vpn_users',
    config         => 'user root',
    script_path_in => '/usr/local/share/munin-plugins';
  }

  service { 'openvpn':
    ensure    => true,
    restart   => '/etc/init.d/openvpn restart',
    pattern   => '/usr/sbin/openvpn',
    subscribe => [ File['/etc/openvpn/vpnred-service.conf'] ];
  }

  check_mk::agent::ps {
    'openvpn':
      procname => '~/usr/sbin/openvpn',
      levels   => '2,3,5,6';
  }

  ##
  ## ABLOCKING WEB SERVER
  ##
  apache::vhost::file {
    'vpnred':
      vhost_source => 'modules/vpnred/adblock/apache/blockeverything.conf';
  }

  class { 'webhost':
    php          => false,
    mysql_server => false,
    mysql_client => false,
    ssl          => false,
  }

  apache::module { [ 'authz_core','authz_host','mpm_prefork' ]:
    ensure => present,
  }

  file {
    '/var/www/null':
      ensure  => directory,
      recurse => true,
      owner   => root,
      group   => root,
      mode    => '0644',
      source  => 'puppet:///modules/vpnred/adblock/apache/null';
  }
}
