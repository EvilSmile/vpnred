#!/bin/sh
# THIS FILE IS MANAGED BY PUPPET

#
# remove openvpn rules
#

iptables -t nat -D PREROUTING -p udp --dst <%= @ipaddress %> --dport 443 -j REDIRECT --to-ports 1194  2>/dev/null
iptables -t nat -D PREROUTING -p udp --dst <%= @ipaddress %> --dport 80  -j REDIRECT --to-ports 1194  2>/dev/null
iptables -t nat -D PREROUTING -p udp --dst <%= @ipaddress %> --dport 53  -j REDIRECT --to-ports 1194  2>/dev/null

iptables -t nat -D PREROUTING -p tcp --dst <%= @ipaddress %> --dport 443 -j REDIRECT --to-ports 1194  2>/dev/null
iptables -t nat -D PREROUTING -p tcp --dst <%= @ipaddress %> --dport 80  -j REDIRECT --to-ports 1194  2>/dev/null
iptables -t nat -D PREROUTING -p tcp --dst <%= @ipaddress %> --dport 53  -j REDIRECT --to-ports 1194  2>/dev/null

